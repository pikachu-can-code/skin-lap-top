<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <?php
    $level = "";
    include("config.php");

    include($level . Header_part . "link_header.php");
    ?>
</head>

<body>
    <?php
    //header-start
    include($level . Header_part . "top_header.php");

    //<!-- bradcam_area  -->
    include($level . Single_Blog_part . "single_blog_area.php");

    //Blog Area
    include($level . Single_Blog_part . "single_blog_area_bandicam.php");

    //From Comment
    include($level . Single_Blog_part . "single_blog_from.php");

    //Category
    include($level . Body_part . "category.php");

    // <!-- footer start -->
    include($level . Footer_part . "footer.php");

    //<!-- JS here -->
    include($level . Footer_part . "JS.php");
    ?>



</body>

</html>