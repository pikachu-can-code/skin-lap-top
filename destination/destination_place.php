<div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="img/place/sanpham1.png" alt="">
                                    <a href="#" class="prise">28.990.000₫</a>
                                </div>
                                <div class="place_info">
                                    <a href="destination_details.html">
                                        <h3>Laptop Apple Macbook  2019</h3>
                                    </a>
                                    <p>i5 1.6GHz/8GB/128GB</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <a href="#">(20 lượt thích)</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="#">6 ngày</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="img/place/sanpham2.png" alt="">
                                    <a href="#" class="prise">26.490.000₫</a>
                                </div>
                                <div class="place_info">
                                    <a href="destination_details.html">
                                        <h3>Laptop Dell Inspiron 7591</h3>
                                    </a>
                                    <p>i5 9300H/8GB/256GB/3GB</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <a href="#">(10 lượt thích)</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="#">5 ngày</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="img/place/sanpham3.png" alt="">
                                    <a href="#" class="prise">23.990.000₫</a>
                                </div>
                                <div class="place_info">
                                    <a href="destination_details.html">
                                        <h3>Laptop Asus ZenBook UX533FD</h3>
                                    </a>
                                    <p>i5 8265U/8GB/256GB/2GB</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <a href="#">(25 lượt thích)</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="#">5 ngày</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="img/place/sanpham4.png" alt="">
                                    <a href="#" class="prise">29.990.000₫</a>
                                </div>
                                <div class="place_info">
                                    <a href="destination_details.html">
                                        <h3>Laptop HP Envy 13</h3>
                                    </a>
                                    <p>i7 10510U/8GB/512GB/2GB</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <a href="#">(12 lượt thích)</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="#">4 ngày</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="img/place/sanpham5.png" alt="">
                                    <a href="#" class="prise">51.990.000₫</a>
                                </div>
                                <div class="place_info">
                                    <a href="destination_details.html">
                                        <h3>Laptop HP EliteBook X360</h3>
                                    </a>
                                    <p>i7 8550U/16GB/512GB/Win10</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <a href="#">(49 lượt thích)</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="#">3 ngày</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="img/place/sanpham6.png" alt="">
                                    <a href="#" class="prise">56.990.000₫</a>
                                </div>
                                <div class="place_info">
                                    <a href="destination_details.html">
                                        <h3>Laptop Macbook Pro Touch</h3>
                                    </a>
                                    <p>i7 2.6GHz/16GB/512GB/4GB</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <a href="#">(22 lượt thích)</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="#">4 ngày</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="more_place_btn text-center">
                                <a class="boxed-btn4" href="#">Xem thêm 19 laptop</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>