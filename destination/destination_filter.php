



    <div class="popular_places_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="filter_result_wrap">
                        <h3>Khuyến mãi</h3>
                        <div class="filter_bordered">
                            <div class="filter_inner">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="single_select">
                                            <select>
                                                <option data-display="Quà tặng">Quà tặng</option>
                                                <option value="1">Balo Laptop</option>
                                                <option value="2">Giảm 10% khi mua tai nghe</option>
                                                <option value="3">Giảm ngay 10% (đã trừ vào giá)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="single_select">
                                            <select>
                                                <option data-display="Dịch vụ">Dịch vụ</option>
                                                <option value="1">Giao ngay từ cửa hàng gần bạn nhất</option>
                                                <option value="2">Áp dụng khi mua trả góp 0%</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="reset_btn">
                                <button class="boxed-btn4" type="submit">Áp dụng</button>
                            </div>
                        </div>
                    </div>
                </div>