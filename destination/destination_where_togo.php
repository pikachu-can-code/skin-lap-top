
    <!-- where_togo_area_start  -->
    <div class="where_togo_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="form_area">
                        <h3>Tìm kiếm sản phẩm</h3>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="search_wrap">
                        <form class="search_form" action="#">

                            <div class="input_field">
                                <select>
                                    <option data-display="Thương hiệu">Thương hiệu</option>
                                    <option value="1">Apple</option>
                                    <option value="2">Dell</option>
                                    <option value="3">Asus</option>
                                    <option value="4">Lenovo</option>
                                    <option value="5">Acer</option>
                                </select>
                            </div>
                            <div class="input_field">
                                <select>
                                    <option data-display="Giá thành">Giá thành</option>
                                    <option value="1">Dưới 10 triệu</option>
                                    <option value="2"> 10 - 15 triệu</option>
                                    <option value="3">15 - 25 triệu</option>
                                    <option value="4">Trên 25 triệu</option>
                                </select>
                            </div>
                            <div class="input_field">
                                <select>
                                    <option data-display="Tính năng">Tính năng</option>
                                    <option value="1">Bảo mật vân tay</option>
                                    <option value="2">Sạc pin nhanh</option>
                                    <option value="3">Có đèn bàn phím</option>
                                </select>
                            </div>
                            <div class="search_btn">
                                <button class="boxed-btn4 " type="submit">Tìm kiếm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- where_togo_area_end  -->