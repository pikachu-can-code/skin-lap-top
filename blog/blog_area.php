 <!--================Blog Area =================-->
 <section class="blog_area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/vuottroi1.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>11</h3>
                                    <p>Jan</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Intel thế hệ 10 cực mạnh</h2>
                                </a>
                                <p>Tận hưởng chơi game với FPS (tốc độ khung hình) cao và đáng kinh ngạc ngay cả khi đang phát trực tuyến và ghi với tốc độ lên tới 5.3 GHz. Tăng tốc bộ nhớ Turbo và Intel® Optane™. Tại nhà và lưu động, bộ xử lý Intel® Core™ Thế hệ thứ 10 có thể ép xung cung cấp năng lượng cho các giàn máy tính xách tay chơi game đỉnh cao.</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i> HCM, Huy Hoàng</a></li>
                                    <li><a href="#"><i class="fa fa-comments"></i> 03 Bình luận</a></li>
                                </ul>
                            </div>
                        </article>

                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/vuottroi2.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>15</h3>
                                    <p>Jan</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Bàn phím hào quang Aura rực rỡ bất cứ đâu</h2>
                                </a>
                                <p>Hãy tắt bớt đèn điện và nhấn nút nguồn, giai điệu huyền diệu của bài ca ánh sáng sẽ nổi lên theo từng nhịp gõ phím của bạn. Nói cách khác, dải ánh sáng và đèn nền RGB được tự động hóa hoặc tùy biến bởi người dùng sẽ phát sáng theo cách mà bạn mong muốn với hệ thống ánh sáng Aura Sync và Aura Creator.

Bên cạnh đó, 4 phím chơi game huyền thoại WASD được cách điệu một cách tinh tế, giúp bạn di chuyển và spam skills thoải mái, tiêu diệt kẻ địch trong chớp mắt. Bật mí thêm cho bạn là độ bền của các phím lên đến 20 triệu lần và không bị giảm chất lượng khi bị ‘spam’ liên tục.</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i> HCM, Phi Khanh</a></li>
                                    <li><a href="#"><i class="fa fa-comments"></i> 02 Bình luận</a></li>
                                </ul>
                            </div>
                        </article>

                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/vuottroi6.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>18</h3>
                                    <p>Jan</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Cấu hình bất khả chiến bại</h2>
                                </a>
                                <p>Laptop ASUS ROG Strix G G531GT-AL017T sở hữu con Chip Intel Core i7 thế hệ thứ 9 dòng H mạnh mẽ, cùng với đó là ổ cứng SSD M.2 khủng 512 GB, đem đến tốc độ xử lý dữ liệu siêu nhanh siêu tốc độ. Nói rõ hơn, bạn có thể khởi động Windows 10 chỉ với 10 giây và thời gian load ứng dụng được giảm thiểu bất ngờ.</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i> HCM , Vĩ Khang</a></li>
                                    <li><a href="#"><i class="fa fa-comments"></i> 03 Bình luận</a></li>
                                </ul>
                            </div>
                        </article>

                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/vuottroi3.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>19</h3>
                                    <p>Jan</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Card đồ họa mạnh mẽ, nâng tầm giải trí</h2>
                                </a>
                                <p>Với sự trợ giúp của card đồ họa rời NVIDIA® GeForce GTX™ 1650 4 GB và RAM tối đa lên đến 32 GB, chiếc laptop này có thể chiến được game khủng như Apex Lengends, GTA 5, The Witcher. Hay thậm chí xử lý thiết kế 3D trên Photoshop, Corel cũng không thành vấn đề.</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i> HCM, Huy Hoàng</a></li>
                                    <li><a href="#"><i class="fa fa-comments"></i> 05 Bình luận</a></li>
                                </ul>
                            </div>
                        </article>

                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="img/blog/vuottroi5.png" alt="">
                                <a href="#" class="blog_item_date">
                                    <h3>21</h3>
                                    <p>Jan</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="single-blog.html">
                                    <h2>Âm thanh sống động</h2>
                                </a>
                                <p>Laptop gaming Acer Nitro AN515 có hệ thống âm thanh được xử lý Waves MaxxAudio và Acer TrueHarmony lọc bỏ bớt tiếng ồn mang đến những trải nghiệm âm thanh sống động nhất cho người dùng.</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i> HCM, Phi Khanh</a></li>
                                    <li><a href="#"><i class="fa fa-comments"></i> 03 Bình luận</a></li>
                                </ul>
                            </div>
                        </article>

                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Previous">
                                        <i class="ti-angle-left"></i>
                                    </a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link">1</a>
                                </li>
                                <li class="page-item active">
                                    <a href="#" class="page-link">2</a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Next">
                                        <i class="ti-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>