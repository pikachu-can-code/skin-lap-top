<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <?php
    $level = "";
    include("config.php");

    include($level . Header_part . "link_header.php");
    ?>
</head>

<body>
    <?php
    //header-start
    include($level . Header_part . "top_header.php");

    //<!-- bradcam_area  -->
    include($level . Destination_Details_part . "destination_details_area.php");

    //Description
    include($level . Destination_Details_part . "destination_details_description.php");

    // <!-- newletter_area_start  -->
    include($level . Destination_part . "destination_newletter.php");

    //More Places
    include($level . Destination_Details_part . "destination_details_MorePlaces.php");

    // <!-- footer start -->
    include($level . Footer_part . "footer.php");

    //<!-- JS here -->
    include($level . Footer_part . "JS.php");
    ?>
</body>

</html>