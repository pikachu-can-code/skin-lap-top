<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <?php
    $level = "";
    include("config.php");

    include($level . Header_part . "link_header.php");
    ?>
</head>

<body>
    <?php
    //header-start
    include($level . Header_part . "top_header.php");

    //<!-- bradcam_area  -->
    include($level . Index_part . "index_area.php");

    // <!-- where_togo_area_start  -->
    include($level . Destination_part . "destination_where_togo.php");
    
    //<!-- bradcam_area  -->
    include($level . Index_part . "index_Popular_Brands.php");

    // <!-- newletter_area_start  -->
   include($level . Destination_part . "destination_newletter.php");

//    //Popular_Places
//    include($level . Index_part . "index_Popular_Places.php");


    //<!-- enjoy Video  -->
    include($level . About_part . "about_enjoy.php");

     // <!-- recent_trip  -->
     include($level . Destination_part . "destination_recent_trip.php");


     // <!-- footer start -->
     include($level . Footer_part . "footer.php");
 
     //<!-- JS here -->
     include($level . Footer_part . "JS.php");
    ?>
</body>

</html>