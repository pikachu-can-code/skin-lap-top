 <!-- slider_area_start -->
 <div class="slider_area">
     <div class="slider_active owl-carousel">
         <div class="single_slider d-flex align-items-center slider_bg_1 overlay">
             <div class="container">
                 <div class="row align-items-center">
                     <div class="col-xl-12 col-md-12">
                         <div class="slider_text text-center">
                             <h3>Surface Laptop 3</h3>
                             <p>
                                 Với chip xử lý intel thế hệ thứ 7 đi cùng màn hình 12.3 inch độ phân giải 4K (
                                 2736 x 1824 Pixels) tích hợp công nghệ màn hình Pixel Sense
                             </p>
                             <a href="#" class="boxed-btn3">Khám phá ngay</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="single_slider d-flex align-items-center slider_bg_2 overlay">
             <div class="container">
                 <div class="row align-items-center">
                     <div class="col-xl-12 col-md-12">
                         <div class="slider_text text-center">
                             <h3>Dell XPS 13 2020</h3>
                             <p>
                                 Với thiết kế hiện đại đẹp hơn, tỷ lệ màn hình lớn hơn và viền màn hình mỏng hơn
                             </p>
                             <a href="#" class="boxed-btn3">Khám phá ngay</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="single_slider d-flex align-items-center slider_bg_3 overlay">
             <div class="container">
                 <div class="row align-items-center">
                     <div class="col-xl-12 col-md-12">
                         <div class="slider_text text-center">
                             <h3>Laptop HP 348 G5 (7XU21PA)</h3>
                             <p>
                                 Laptop HP 348 G5 7XU27PA mang thiết kế hiện đại và trang nhã. Với lớp vỏ bọc làm
                                 từ nhựa khiến trọng lượng của máy nhẹ chỉ 1,63kg.
                             </p>
                             <a href="#" class="boxed-btn3">Khám phá ngay</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!-- slider_area_end -->

 