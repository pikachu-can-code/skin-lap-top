<!-- testimonial_area  -->
<div class="about_story">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="story_heading">
                        <h3>ĐỘI NGŨ NHÂN SỰ</h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-11 offset-lg-1">
                            <div class="story_info">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <p>Với đội ngũ nhân sự hùng hậu và dạn dày kinh nghiệm. Tại GrearVn, tất cả các thành viên đều đáp ứng các yêu cầu về trình độ chuyên môn (phù hợp từng vị trí công việc), có tinh thần trách nhiệm, đạo đức nghề nghiệp và tính kỷ luật cao.</p>
                                <p>Đội ngũ quản trị tại GrearVn là những người có tầm vóc về tư duy, có kinh nghiệm trên thương trường và có tâm với sự phát triển chung của doanh nghiệp, xã hội, cộng đồng.</p>
                                <p>Đội ngũ nhân sự GrearVn là những người trẻ đầy hoài bão và khát khao; tự tin; năng động và hết lòng với công việc, với khách hàng.</p>
                                <p>Tất cả đã hợp thành một đội ngũ mạnh cùng chung ý chí, quyết tâm và cùng vì mục tiêu chung: Đưa GrearVn phát triển vững mạnh, vươn xa!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="story_thumb">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="thumb padd_1">
                                            <img src="img/about/nhansu1.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="thumb padd_1">
                                            <img src="img/about/nhansu2.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="counter_wrap">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="single_counter text-center">
                                            <h3  class="counter">378</h3>
                                            <p>Sản phẩm bán được hàng tháng</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="single_counter text-center">
                                            <h3 class="counter">30</h3>
                                            <p>Số event đã được tổ chức</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="single_counter text-center">
                                            <h3 class="counter">2263</h3>
                                            <p>Số người theo dõi</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
