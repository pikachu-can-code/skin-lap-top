<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <?php
    $level = "";
    include("config.php");

    include($level . Header_part . "link_header.php");
    ?>
</head>

<body>
    <?php
    //header-start
    include($level . Header_part . "top_header.php");

    //<!-- bradcam_area  -->
    include($level . Elements_part . "elements_area.php");

    //Button
    include($level . Elements_part . "elements_button.php");

    //<!-- Start Align Area -->
    include($level . Elements_part . "elements_align.php");

    // <!-- footer start -->
    include($level . Footer_part . "footer.php");

    //<!-- JS here -->
    include($level . Footer_part . "JS.php");
    ?>
</body>

</html>