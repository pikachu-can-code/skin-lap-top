$(document).ready(function() {

    (function($) {
        "use strict";


        jQuery.validator.addMethod('answercheck', function(value, element) {
            return this.optional(element) || /^\bcat\b$/.test(value)
        }, "type the correct answer -_-");

        // validate contactForm form
        $(function() {
            $('#contactForm').validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 2
                    },
                    subject: {
                        required: true,
                        minlength: 4
                    },
                    number: {
                        required: true,
                        minlength: 5
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 20
                    }
                },
                messages: {
                    name: {
                        required: "Hãy nhập tên của bạn!",
                        minlength: "Tên của bạn phải có ít nhất 2 ký tự!"
                    },
                    subject: {
                        required: "Hãy nhập chủ đề bạn muốn gửi đến chúng tôi!",
                        minlength: "Chủ đề của bạn phải có ít nhất 4 ký tự"
                    },
                    number: {
                        required: "Hãy nhập số điện thoại của bạn!",
                        minlength: "Số của bạn phải có ít nhất 10 chữ số"
                    },
                    email: {
                        required: "Hãy nhập email của bạn!"
                    },
                    message: {
                        required: "Bạn phải viết gì đó để gửi vào mẫu đơn này!",
                        minlength: "Đó là tất cả phải không!"
                    }
                },
                submitHandler: function(form) {
                    $(form).ajaxSubmit({
                        type: "POST",
                        data: $(form).serialize(),
                        url: "contact_process.php",
                        success: function() {
                            $('#contactForm :input').attr('disabled', 'disabled');
                            $('#contactForm').fadeTo("slow", 1, function() {
                                $(this).find(':input').attr('disabled', 'disabled');
                                $(this).find('label').css('cursor', 'default');
                                $('#success').fadeIn()
                                $('.modal').modal('hide');
                                $('#success').modal('show');
                            })
                        },
                        error: function() {
                            $('#contactForm').fadeTo("slow", 1, function() {
                                $('#error').fadeIn()
                                $('.modal').modal('hide');
                                $('#error').modal('show');
                            })
                        }
                    })
                }
            })
        })

    })(jQuery)
})